# Spotippos - anúncios

Viva Real code challenge

## Getting Started


### Prerequisities

* git
* node 4.2.4 +

Clonning the repository
```
git clone https://gitlab.com/klaytonfaria/spotippos-anuncios.git
```

### Installing


Run code bello to install dependencies:

```
npm install
```

### Running
Run on dev mode:
```
npm start
```

### Build
To build files run:
```
npm run build
```

### Serving
To serve built files run:
```
npm run serve
```

## Deployment

To deploy just push any tag and will be deployed on pipeline.

### TODOS

- [x] Internationalization pt-BR
- [x] Internationalization en-US
- [x ] Missing filters behaviors
- [ ] Code review
- [ ] Behaviors and layout review
- [ ] Mobile version
- [ ] End to end tests
