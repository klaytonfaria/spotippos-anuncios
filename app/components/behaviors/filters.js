'use strict';

// Stores
import PropertiesStore from '../../flux/stores/PropertiesStore';

export let filter = (filterList) => {
  var propertiesList = PropertiesStore.data.properties;

  filterList.map((filterItem) => {
    let filterName = filterItem[0],
        filterValue = filterItem[1],
        result = false;

    // Filtering objects
    propertiesList = propertiesList.filter(function(item) {
      switch (filterName) {
        case 'squareMeters':
          result = item.squareMeters === filterValue
        break;
        case 'beds':
          result = item.beds === filterValue
        break;
        case 'baths':
          result = item.baths === filterValue
        break;
        case 'minPrice':
          result = (parseInt(filterValue, 10) <= parseInt(item.price, 10))
        break;
        case 'maxPrice':
          result = (parseInt(filterValue, 10) >= parseInt(item.price, 10))
        break;
        default:
          result = false
        break;
      }
      return result;
    });
  });

  PropertiesStore.filteredData = {
    properties: propertiesList,
    foundProperties: propertiesList.length
  }

  PropertiesStore.emitChangeByFilter();
};
