'use strict';
import Mask from 'vanilla-masker';

export let mask = (selector) => {
  let elems = document.querySelectorAll(selector);

  let numbers = () => {
    Mask(elems).maskNumber()
  };

  let money = () => {
    Mask(elems).maskMoney({
      precision: 0,
      separator: ',',
      delimiter: '.',
      unit: app.intl.formatMessage({id: 'app.money.unit'}),
      zeroCents: false
    });
  }

  let destroy = () => {
    Mask(elems).unMask();
  }

  let cleanOnScape = () => {
    elems.forEach((elem) => {
      elem.addEventListener('keyup', (e) => {
        var code = e.charCode || e.keyCode;
        if (code == 27) {
            elem.value = '';
        }
      }, false);
    });
  }

  let toNumber = Mask.toNumber;

  return {
    numbers,
    money,
    destroy,
    toNumber,
    cleanOnScape
  }
}
