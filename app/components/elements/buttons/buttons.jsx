'use strict';

import React, {Component} from 'react';
import {Link} from "react-router";

// Styles
import style from './styles/buttons';

class ButtonLink extends Component {
  render() {
    let icon = this.props.icon ? <span className={`icon ${this.props.icon}`}></span> : '';
    return (
      <Link to={this.props.href} title={this.props.label} className={`button ${this.props.className}`}>
        {icon} <span className='label'>{this.props.label}</span>
      </Link>
    );
  }
}

export default ButtonLink;
