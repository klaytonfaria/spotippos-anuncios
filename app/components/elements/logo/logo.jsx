'use strict';

import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import { FormattedMessage } from 'react-intl';
import {Link} from "react-router";

// Styles
import style from './styles/logo';

class Logo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      useLink: this.props.href !== undefined
    };
  }

  componentDidMount() {
    this.setState({
      useLink: this.props.href !== undefined
    });
  }

  render() {
    let logo;

    if(this.state.useLink) {
      logo = <Link to={this.props.href} title={this.props.title}><span className='icon icon-logo'></span></Link>;
    } else {
      logo = <span className='icon icon-logo'></span>;
    }

    return (
      <h1 className='logo'>{logo}</h1>
    );
  }
}

export default Logo;
