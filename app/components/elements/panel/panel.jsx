'use strict';

import React, {Component} from 'react';

// Styles
import style from './styles/panel';

class Panel extends Component {
  constructor(props) {
    super(props);
    this.children = React.PropTypes.element;
  }
  render() {
    return (
      <div id={this.props.id} className={`panel ${this.props.className}`}>
        {this.props.children}
      </div>
    );
  }
}

export default Panel;
