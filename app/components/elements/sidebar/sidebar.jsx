'use strict';

import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';

// Styles
import style from './styles/sidebar';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.children = React.PropTypes.element;
    this.state = {
      collapsed: false
    };
  }
  render() {
    let isCollapsed = this.state.collapsed ? "collapsed" : "";

    return (
      <aside id="sidebar" className={`sidebar ${this.props.className} ${isCollapsed}`}>
        {this.props.children}
      </aside>
    );
  }
}

export default Sidebar;
