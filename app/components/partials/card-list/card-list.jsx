'use strict';

import React, {Component} from 'react';

// Componets
import Card from '../../partials/card/card';

// Styles
import style from './styles/card-list';

class CardList extends Component {

  render() {
    let data = this.props.data || {},
        component = [];

    if(data.hasOwnProperty("properties") && data.properties.length > 0) {
      component = data.properties.map(function(item, i) {
        return (
          <Card key={`item-${i}`} data={item}/>
        )
      })
    } else if(data.id) {
      component = <Card key='item-0' data={data}/>
    }
    return (
      <div className='card-list scroll'>
        {component}
      </div>
    );
  }
}

export default CardList;
