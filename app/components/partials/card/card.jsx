'use strict';

import React, {Component} from 'react';
import {FormattedMessage, FormattedNumber} from 'react-intl';

// Componets
import Panel from '../../elements/panel/panel';
import ButtonLink from '../../elements/buttons/buttons';

// Styles
import style from './styles/card';

class Card extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Panel className='card light full'>
        <div className='card-images'>
          <span className='card-price'>
            <FormattedMessage id="app.money.currency">
              {(currency) => <FormattedNumber value={this.props.data.price} style='currency' currency={currency} />}
            </FormattedMessage>
            </span>
          <span className='icon icon-building image-placeholder'></span>
          <figure className='card-image'>
            {/* <img src={this.props.data.image} alt={this.props.data.title}/> */}
            <img src='http://resizedimgs.vivareal.com/fit-in/870x653/vr.images.sp/871543223011b600aef09e9d6b863f37.jpg' alt={this.props.data.title}/>

          </figure>
        </div>
        <div className='card-wrap'>
          <section className='card-info'>
            <span className='card-id'><FormattedMessage id='advertisement.id'/>{this.props.data.id}</span>
            <h2 className='card-title'>{this.props.data.title}</h2>
            <p className='card-description'>{this.props.data.description}</p>
          </section>
          <div className='card-footer'>
            <ul className='card-specifications'>
              <li className='card-specification-item'>
                <span className='icon icon-area'></span>
                <span className='label'>{this.props.data.squareMeters} M²</span>
              </li>
              <li className='card-specification-item'>
                <span className='icon icon-beds'></span>
                <span className='label'><FormattedMessage id='advertisement.bedroom' values={{total: this.props.data.beds}} /></span>
              </li>
              <li className='card-specification-item'>
                <span className='icon icon-bathroom'></span>
                <span className='label'><FormattedMessage id='advertisement.bathroom' values={{total: this.props.data.baths}} /></span>
              </li>
            </ul>
            <div className='card-actions'>
              <FormattedMessage id="advertisement.view">
                {(message) => <ButtonLink href='/' label={message} title={message} className='view-more'/>}
              </FormattedMessage>
            </div>
          </div>
        </div>
      </Panel>
    );
  }
}

export default Card;
