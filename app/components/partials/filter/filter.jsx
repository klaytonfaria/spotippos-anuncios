'use strict';

import React, {Component} from 'react';
import { FormattedMessage } from 'react-intl';

// Componets
import Panel from '../../elements/panel/panel';
import FilterActions from '../../../flux/actions/FilterActions';
import {mask} from '../../behaviors/forms';

// Styles
import style from './styles/filter';

let toggleFiltersOnFiltering = (flag) => {
  let disable = flag;
  document.querySelectorAll('#filter input').forEach((elem) => {
    if(elem.id !== 'filterById') {
      elem.disabled = disable;
    }
  });
}

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: []
    }
  }

  componentDidMount() {
    mask('.mask-number').numbers();
    mask('.mask-money').money();
    mask('#filterForm input').cleanOnScape();

  }


  handleFilterById() {
    let val = document.getElementById('filterById').value;
    FilterActions.filterById(val);
    toggleFiltersOnFiltering(val);
  }

  handleFilter() {
    let filterList = [];
    document.querySelectorAll('#filterForm input').forEach(function(filter) {
      let value = mask().toNumber(filter.value);
      if(filter.name && filter.name !== 'id' && value !== '') {
        filterList.push([filter.name, value]);
      }
    })
    FilterActions.filter(filterList);
  }


  render() {
    return (
      <Panel id='filter' className='filter-box no-horizontal-padding snap-bottom'>
        <form id='filterForm'>
          <span className='filter-title'><FormattedMessage id='search.filter'/></span>
          <div className='filter-section'>
            <div className='field'>
              <label><FormattedMessage id='search.filter.id'/></label>
              <input type="text" name="id" id="filterById" onKeyUp={this.handleFilterById} className='mask-number' />
            </div>
            <div className='field'>
              <label><FormattedMessage id='search.filter.area'/></label>
              <input type="text" name="squareMeters" id="filterByArea" onKeyUp={this.handleFilter.bind(this)} className='mask-number' />
            </div>
          </div>
          <div className='filter-section'>
            <div className='field'>
              <label><FormattedMessage id='search.filter.bedroom'/></label>
              <input type="text" name="beds" id="filterByBeds" onKeyUp={this.handleFilter.bind(this)} className='mask-number'/>
            </div>
            <div className='field'>
              <label><FormattedMessage id='search.filter.bathroom'/></label>
              <input type="text" name="baths" id="filterByArea" onKeyUp={this.handleFilter.bind(this)} className='mask-number'/>
            </div>
          </div>
          <div className='filter-section'>
            <div className='field'>
              <label><FormattedMessage id='search.filter.price'/></label>
              <FormattedMessage id="search.filter.price.min">
                {(message) => <input type="text" name="minPrice" id="filterByMinPrice" onKeyUp={this.handleFilter.bind(this)} placeholder={message} className='mask-money' />}
              </FormattedMessage>
            </div>
            <span className="join"></span>
            <div className='field'>
              <label></label>
              <FormattedMessage id="search.filter.price.max">
                {(message) => <input type="text" name="maxPrice" id="filterByMaxPrice" onKeyUp={this.handleFilter.bind(this)}  placeholder={message} className='mask-money' />}
              </FormattedMessage>
            </div>
          </div>
        </form>
      </Panel>
    );
  }
}

export default Filter;
