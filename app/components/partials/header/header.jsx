'use strict';

import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import { FormattedMessage } from 'react-intl';

// Componets
import Logo from '../../elements/logo/logo';

// Styles
import style from './styles/header';

class Header extends Component {
  render() {
    return (
      <header>
        <Logo title="Viva Real" href="/" />
        <div className='header-title'>
          <FormattedMessage id='app.title' />
        </div>
      </header>
    );
  }
}

export default Header;
