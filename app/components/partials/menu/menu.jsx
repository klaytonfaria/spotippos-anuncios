'use strict';

import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import {FormattedMessage} from 'react-intl';
import {Link} from "react-router";


// Styles
import style from './styles/menu';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: this.props.collapsed
    };
  }
  render() {
    let formatMessage = this.props.intl.formatMessage,
        isCollapsed = this.state.collapsed ? "collapsed" : "",
        menuData = this.props.data || {items:[]};

    let menuItems = menuData.items.map(function(data) {
      let isActive = data.active ? "active" : "";
      return (
        <Link to={data.url} key={data.label} title={data.label} className={`menu-item ${data.classNames} ${isActive}`}>
          <span className={`icon ${data.icon}`}></span>
          <span className='label'>{formatMessage({id: data.label})}</span>          
        </Link>
      );
    });

    return (
      <section id='menu' className={`menu ${this.props.className} ${isCollapsed}`}>
        {menuItems}
      </section>
    );
  }
}

export default Menu;
