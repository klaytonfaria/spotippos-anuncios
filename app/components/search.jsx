'use strict';

import React, {Component} from 'react';
import {FormattedMessage} from 'react-intl';

// Stores
import PropertiesStore from '../flux/stores/PropertiesStore';

// Components
import Filter from './partials/filter/filter';
import Panel from './elements/panel/panel';
import CardList from './partials/card-list/card-list';

class SearchPage extends Component {
  constructor(props) {
    super(props);
    PropertiesStore.fetchAll();
    this.state = {
      data: {},
      noresults: true
    };
  }

  onChange() {
    let data = PropertiesStore.getData();
    if(data.foundProperties > 0 || data.id) {
      this.setState({
        data: data,
        noresults: false,
      });
    } else {
      this.setState({
        noresults: true
      });
    }
  }

  onFilter() {
    let filteredData = PropertiesStore.getFilteredData();    
    if(filteredData.foundProperties > 0) {
      this.setState({
        data: filteredData,
        noresults: false
      });
    } else {
      this.setState({
        noresults: true
      });
    }

  }

  componentWillMount() {
    PropertiesStore.addChangeListener(() => this.onChange());
    PropertiesStore.addChangeByFilterListener(() => this.onFilter());
  }

  componentWillUnmount() {
    PropertiesStore.removeChangeListener(() => this.onChange());
    PropertiesStore.removeChangeByFilterListener(() => this.onFilter());
  }

  render() {
    let content = <CardList data={this.state.data}/>;

    if(this.state.noresults) {
      content = <div className='no-result'><span className='icon'>:'(  </span><FormattedMessage id='search.filter.noresult'/></div>;
    }

    return (
      <div className='page-main search-page'>
        <Filter {...this.props} />
        <Panel id='search-results' className='search-results light fluid snap-bottom'>
          {content}
        </Panel>
        {}
      </div>
    );
  }
}

export default SearchPage;
