'use strict';

import React, {Component} from 'react';
import {injectIntl} from 'react-intl';

// Styles
import Styles from './styles/default';

// Componets
import Header from '../../partials/header/header';
import Sidebar from '../../elements/sidebar/sidebar';
import Menu from '../../partials/menu/menu';

class Page extends Component {
  render() {
    // TODO: Get from service
    let menuData = {
      items : [
        {
          'icon': 'icon-building',
          'label': 'menu.item.advertisement',
          'url': '/',
          'classNames': '',
          'active': true
        },
        {
          'icon': 'icon-plus',
          'label': 'menu.item.create-advertisement',
          'url': '/',
          'classNames': '',
          'active': false
        }
      ]
    };

    const childrenWithProps = React.Children.map(this.props.children,
     (child) => React.cloneElement(child, {
       intl: this.props.intl
     })
    );

    window.app = window.app || {};
    window.app.intl = this.props.intl;
    return (
      <div className='page-wrap'>
          <Header/>
          <div className="page-content">
            <Sidebar className='left' collapsed={false} intl={this.props.intl}>
              <Menu className="vertical" collapsed={false} data={menuData} intl={this.props.intl} />
            </Sidebar>
            {childrenWithProps}
          </div>
      </div>
    );
  }
}

export default injectIntl(Page);
