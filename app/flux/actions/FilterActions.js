'use strict';

import AppDispatcher from '../dispatcher/AppDispatcher';

let FilterActions = {
  filterById: function (id) {
    AppDispatcher.dispatch({
      actionType: 'FILTER_BY_ID',
      id: id
    });
  },
  filter: function (filterList) {
    AppDispatcher.dispatch({
      actionType: 'FILTER',
      filterList: filterList
    });
  },
};

export default FilterActions;
