'use strict';

import {Dispatcher} from 'flux';
import PropertiesStore from '../stores/PropertiesStore';

let AppDispatcher = new Dispatcher();

AppDispatcher.register(function (action) {
  switch(action.actionType) {
    case 'FILTER_BY_ID':
      PropertiesStore.fetchById(action.id);
    break;
    case 'FILTER':
      PropertiesStore.filter(action.filterList);
    break;
  }
})

module.exports = AppDispatcher;
