'use strict';

import {API_URL} from '../../config/constants';
import {EventEmitter} from 'events';
import {default as assign} from 'object-assign';
import {filter} from '../../components/behaviors/filters';

let fetchData = (id) => {
  let allDataUrl = `${API_URL}?ax=1&ay=1&bx=100&by=100`,
      url = id && typeof Number(id) === 'number' ? `${API_URL}/${id}` : allDataUrl;

  fetch(url)
    .then((res) => res.json())
    .then((res) => {
      PropertiesStore.data = res;
      PropertiesStore.emitChange();
    });
};



let PropertiesStore = assign({}, EventEmitter.prototype, {
  data: {},
  filteredData: {},
  fetchById: (id) => {fetchData(id)},
  fetchAll: () => fetchData(),
  getData: function () {
    return this.data;
  },
  getFilteredData: function () {
    return this.filteredData;
  },
  filter: (filterList) => {
    filter(filterList);
  },
  emitChange: function () {
    this.emit('change');
  },
  emitChangeByFilter: function () {
    this.emit('changeByFilter');
  },
  addChangeListener: function (callback) {
    this.on('change', callback);
  },
  addChangeByFilterListener: function (callback) {
    this.on('changeByFilter', callback);
  },
  removeChangeListener: function (callback) {
    this.removeListener('change', callback);
  },
  removeChangeByFilterListener: function (callback) {
    this.removeListener('changeByFilter', callback);
  }
});

export default PropertiesStore;
