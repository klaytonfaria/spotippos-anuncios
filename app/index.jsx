'use strict';

import React, {Component} from 'react';
import ReactDOM, {render} from 'react-dom';
import Routes from './routes';
import {IntlProvider, addLocaleData} from 'react-intl';
import {locale, languages} from './languages';
import spotipposIcons from './resources/icons-font.json';


// App base style
import style from './styles/base';

class App extends Component {
  render() {
    return (
      <IntlProvider locale={'pt-BR'} messages={languages['pt-BR']}>
        {Routes}
      </IntlProvider>
    );
  }
}

render(<App/>, document.getElementById('app'));
