'use strict';

import {addLocaleData, ReactIntlLocaleData} from "react-intl";
import pt from "react-intl/locale-data/pt";
addLocaleData([...pt]);

/**
* Used to load recursively language messages files
*
* @method loadLanguagesFiles
* @example loadLanguagesFiles();
* @return Object
**/

let loadLanguagesFiles = () => {
  let languagesFiles = require.context('./i18n', true, /\.json.*$/),
      languages = {};

  languagesFiles.keys().forEach(function(file) {
    let locale = file.replace(/(\.\/)?(\.json)?/ig,'');
    languages[locale] = languagesFiles(file);
  });

  return languages;
};

/**
* Used to get current navigator language
*
* @method getLocale
* @example getLocale();
* @return String
**/

let getLocale = () => {
  let locale = navigator.language.split("-");
  return locale[1] ? `${locale[0]}-${locale[1].toUpperCase()}` : "en-US";
};

export let locale = getLocale();
export let languages = loadLanguagesFiles();
