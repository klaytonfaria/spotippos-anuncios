'use strict';

import React from 'react';
import {Router, Route, IndexRoute, hashHistory} from 'react-router/umd/ReactRouter';

// Components
import Page from './components/templates/default/page';
import Search from './components/search';

let routes = (
  <Router history={hashHistory}>
    <Route path='/' component={Page}>
      <IndexRoute component={Search} />
    </Route>
  </Router>
);

export default routes;
