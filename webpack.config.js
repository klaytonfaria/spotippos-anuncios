'use strict';
let ExtractTextPlugin = require('extract-text-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    autoprefixer = require('autoprefixer');

module.exports = {
  devServer : {
    contentBase : './app'
  },
  entry: `${__dirname}/app/index`,
  output: {
    path: 'public/',
    filename: 'js/app.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.scss', '.json']
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\-font\.json$/,
        loader: ExtractTextPlugin.extract('css!sass!fontgen')
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('css!postcss!sass')
      },
      {
        test: /\.json/,
        loader: 'json'
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('/style/app.css', {
      allChunks: true
    }),
    new CopyWebpackPlugin([
      {
        from: 'app/index.html',
        to: 'index.html'
      }
    ])
  ],
  postcss: [ autoprefixer({ browsers: ['last 2 versions', 'ie 7-8', 'Firefox > 20'] }) ]
};

// TODO: Create webpack plugin to inject automatic sass variables
